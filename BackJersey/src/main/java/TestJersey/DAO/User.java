/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestJersey.DAO;



import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
  
@Entity
@Table
@XmlType(name = "User")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue
    private int id;
	private String matricule;
	private String login;
	private String pass;
    private String nom;
    private String poste;
    private String mail;
    private Date dateemb;
    
    
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name="role_id" , nullable = false)
    @JsonManagedReference
    private Role role ;


    
    
	public User() {
		super();
	}


	public User(String matricule, String login, String pass, String nom, String poste, String mail, Date dateEmb,
			Role role) {
		super();
		this.matricule = matricule;
		this.login = login;
		this.pass = pass;
		this.nom = nom;
		this.poste = poste;
		this.mail = mail;
		this.dateemb = dateEmb;
		this.role = role;
	}


	public User(int id, String matricule, String login, String pass, String nom, String poste, String mail,
			Date dateEmb, Role role) {
		super();
		this.id = id;
		this.matricule = matricule;
		this.login = login;
		this.pass = pass;
		this.nom = nom;
		this.poste = poste;
		this.mail = mail;
		this.dateemb = dateEmb;
		this.role = role;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getMatricule() {
		return matricule;
	}


	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPoste() {
		return poste;
	}


	public void setPoste(String poste) {
		this.poste = poste;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public Date getDateEmb() {
		return dateemb;
	}


	public void setDateEmb(Date dateEmb) {
		this.dateemb = dateEmb;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//     @ManyToMany(cascade = CascadeType.ALL)
//    private Set<Privilage> privilages = new HashSet<Privilage>();
// 
 
 
  
//    @XmlTransient
//  public Set<Privilage> getPrivilages() {
//        return privilages;
//    }
//
//    public void setPrivilages(Set<Privilage> privilages) {
//        this.privilages = privilages;
//    }
//   public void addPrivilages(Privilage privilages) {
//        this.privilages.add(privilages);
//        
//    }

}