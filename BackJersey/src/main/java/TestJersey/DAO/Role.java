package TestJersey.DAO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table
@XmlType(name = "Role")
@XmlRootElement
public class Role {

	
	@Id
    @GeneratedValue
    @Column(name="role_id")
	private int id;
	private String Role;

	@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<User> users = new HashSet<User>();
	 	
	public Role() {
		super();
	}

	public Role(int id, String role) {
		super();
		this.id = id;
		Role = role;
	}

	public Role(String role) {
		super();
		Role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}

	public Set<User> getUsers() {
		return users;
	}
	
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	
}
