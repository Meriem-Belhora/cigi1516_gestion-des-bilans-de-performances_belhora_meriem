package TestJersey.DAO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "qualification")
@XmlRootElement
public class Qualification {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int id_qualif;
	private String qualif;
	private int poid;

    @ManyToMany(mappedBy = "qualifs" ,cascade = CascadeType.ALL)
	private Set<Theme> themes = new HashSet<Theme>();

	public int getId_qualif() {
		return id_qualif;
	}

	public void setId_qualif(int id_qualif) {
		this.id_qualif = id_qualif;
	}

	public String getQualif() {
		return qualif;
	}

	public void setQualif(String qualif) {
		this.qualif = qualif;
	}

	public int getPoid() {
		return poid;
	}

	public void setPoid(int poid) {
		this.poid = poid;
	}
    @XmlTransient
	public Set<Theme> getThemes() {
		return themes;
	}

	public void setThemes(Set<Theme> themes) {
		this.themes = themes;
	}
	

}
