package TestJersey.DAO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sony
 */
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
  
@Entity
@Table
@XmlRootElement
public class Privilage implements Serializable {
       
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String privilage;
   
     
    @ManyToMany(mappedBy = "privilages" ,cascade = CascadeType.ALL)
    private Set<User> user =  new HashSet<User>();

    
    public Privilage(String privilage, Set<User> User) { this.privilage = privilage;  this.user = User;}
    public Privilage(String privilage) { this.privilage = privilage; }  
    public Privilage() {}
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privilage)) {
            return false;
        }
        Privilage other = (Privilage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Privilage{" + "id=" + id + ", privilage=" + privilage + ", user=" + user + '}';
    }

    
    /**
     * @param user
     * @return the EditP
     */
   
    
    public void AddUser(User user) {
        this.user.add(user);
    }
    
    public String isPrivilage() {
        return privilage;
    }

    public void setPrivilage(String privilage) {
        this.privilage = privilage;
    }

    @XmlTransient
    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }
    
    
 
 
}