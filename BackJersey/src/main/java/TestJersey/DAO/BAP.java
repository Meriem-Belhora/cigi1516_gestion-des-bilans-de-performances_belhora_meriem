package TestJersey.DAO;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table
@XmlType(name = "BAP")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class BAP {
	
	
	private int id;
	private String Evaluation;
	private Qualification qualif;
	List<Qualification> Evals;
	private String Remarque;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEvaluation() {
		return Evaluation;
	}
	public void setEvaluation(String evaluation) {
		Evaluation = evaluation;
	}
	public Qualification getQualif() {
		return qualif;
	}
	public void setQualif(Qualification qualif) {
		this.qualif = qualif;
	}
	public List<Qualification> getEvals() {
		return Evals;
	}
	public void setEvals(List<Qualification> evals) {
		Evals = evals;
	}
	public String getRemarque() {
		return Remarque;
	}
	public void setRemarque(String remarque) {
		Remarque = remarque;
	}
	
	
	

}
