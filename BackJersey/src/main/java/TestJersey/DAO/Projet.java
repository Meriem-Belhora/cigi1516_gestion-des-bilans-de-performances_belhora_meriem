package TestJersey.DAO;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table
@XmlType(name = "Projet")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Projet {

	
	@Id
    @GeneratedValue
	private int id;
	private List<User> collab;
	private List<User> Encadrants;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name="id_theme" , nullable = false)
    @JsonManagedReference
	private List<Theme> themes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<User> getCollab() {
		return collab;
	}

	public void setCollab(List<User> collab) {
		this.collab = collab;
	}

	public List<User> getEncadrants() {
		return Encadrants;
	}

	public void setEncadrants(List<User> encadrants) {
		Encadrants = encadrants;
	}

	public List<Theme> getThemes() {
		return themes;
	}

	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}

}
