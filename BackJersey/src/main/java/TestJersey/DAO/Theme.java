package TestJersey.DAO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table
@XmlType(name = "theme")
@XmlRootElement
public class Theme {

	@Id
    @GeneratedValue
	private int id_theme;
	private String theme;

	 @ManyToMany(cascade = CascadeType.ALL)
	private Set<Qualification> qualifs = new HashSet<Qualification>();

	public int getId_theme() {
		return id_theme;
	}

	public void setId_theme(int id_theme) {
		this.id_theme = id_theme;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	@XmlTransient
	public Set<Qualification> getQualifs() {
		return qualifs;
	}

	public void setQualifs(Set<Qualification> qualifs) {
		this.qualifs = qualifs;
	}

}
