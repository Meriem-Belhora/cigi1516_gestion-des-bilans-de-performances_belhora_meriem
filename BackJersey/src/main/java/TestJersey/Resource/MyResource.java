package TestJersey.Resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Query;
import org.hibernate.Session;

import TestJersey.DAO.HibernateUtil;
import TestJersey.DAO.Qualification;
import TestJersey.DAO.User;

/**
 * Root resource (exposed at "myresource" path)
 */

@Path("myresource")
public class MyResource {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	// @GET
	// @Produces(MediaType.APPLICATION_JSON)
	// public String getIt() {
	// return "Je suis là";
	// }

	@GET
	@Consumes("text/plain")
	@Produces("application/json")
	@Path("/{login}/{pass}")
	// @JsonIgnoreProperties(ignoreUnknown = true)
	public Response authent(@PathParam("login") String login, @PathParam("pass") String pass) {

		User us=new User();
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.beginTransaction();
		Query query = s.createQuery("from User");
		
		
		@SuppressWarnings("unchecked")
		List<User> l = (List<User>) query.list();

		s.getTransaction().commit();
		for (User user : l) {
			
			if ((user.getLogin()).equals(login) && (user.getPass()).equals(pass)) {
				us=user;
			}

		}
		return Response.status(Status.OK).entity(us).build();

	}
	
	@GET
	@Consumes("text/plain")
	@Produces("application/json")
	@Path("qualif")
	public Response qualif(){
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.beginTransaction();
		Query query = s.createQuery("from Qualification");
		
		
		@SuppressWarnings("unchecked")
		List<Qualification> l = (List<Qualification>) query.list();

		s.getTransaction().commit();
		return Response.status(Status.OK).entity(l).build();
		
	}

}
