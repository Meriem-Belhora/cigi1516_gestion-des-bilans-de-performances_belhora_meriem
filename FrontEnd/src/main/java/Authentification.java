
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import Model.User;

import javax.faces.application.FacesMessage;
import javax.faces.bean.*;

@RequestScoped
@ManagedBean
@SessionScoped
public class Authentification {

	private String login;
	private String password;

	private final HttpServletRequest httpServletRequest;
	private final FacesContext faceContext;
	private FacesMessage facesMessage;

	public Authentification() {

		faceContext = FacesContext.getCurrentInstance();
		httpServletRequest = (HttpServletRequest) faceContext.getExternalContext().getRequest();

	}

	// Getters
	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	// Setters

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// Main function
	public String goTo() {
		// consommation de service
		ClientConfig config = new DefaultClientConfig();
		Client c = Client.create(config);
		String url = "http://localhost:8080/testJersey/webapi/myresource/" + login + "/" + password + "";
		WebResource resource = c.resource(url);
		ClientResponse response = resource.accept("application/json").get(ClientResponse.class);

		User us = response.getEntity(User.class);

		// definition de la session utilisateur
		if (us.getNom() == null) {

			facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Acces incorrecte", null);
			faceContext.addMessage(null, facesMessage);
			return "index";
		} else
			// definir la session de l'utilisateur
			httpServletRequest.getSession().setAttribute("sessionUsuario", us);
		facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Acces correcte", null);
		faceContext.addMessage(null, facesMessage);

		return "dashboard";

	}

}
