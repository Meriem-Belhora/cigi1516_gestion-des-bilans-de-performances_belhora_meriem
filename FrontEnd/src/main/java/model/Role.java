package Model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Role {

	private int id_Role;
	private String role;
	
	

	public Role() {
		super();
	}

	public Role(String role) {
		super();
		this.role = role;
	}

	public Role(int id_Role, String role) {
		super();
		this.id_Role = id_Role;
		this.role = role;
	}

	public int getId_Role() {
		return id_Role;
	}

	public void setId_Role(int id_Role) {
		this.id_Role = id_Role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
