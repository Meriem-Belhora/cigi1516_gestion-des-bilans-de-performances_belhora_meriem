/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author sony
 */
public class Privilage {
    
    private Long id;  
    private String privilage;

    public Privilage() {
    }

    public Privilage(Long id, String privilage) {
        this.id = id;
        this.privilage = privilage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrivilage() {
        return privilage;
    }

    public void setPrivilage(String privilage) {
        this.privilage = privilage;
    }
    
    
    
}
