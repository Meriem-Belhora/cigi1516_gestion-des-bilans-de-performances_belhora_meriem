package Model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
	
	 private int id;
		private String matricule;
		private String login;
		private String pass;
	    private String nom;
	    private String poste;
	    private String mail;
	    private Date dateemb;
	    
	    private Role role ;
	      
	    
	    
		public User() {
			super();
		}
		
		
		public User(String matricule, String login, String pass, String nom, String poste, String mail, Date dateemb,
				Role role) {
			super();
			this.matricule = matricule;
			this.login = login;
			this.pass = pass;
			this.nom = nom;
			this.poste = poste;
			this.mail = mail;
			this.dateemb = dateemb;
			this.role = role;
		}


		public User(int id, String matricule, String login, String pass, String nom, String poste, String mail,
				Date dateemb, Role role) {
			super();
			this.id = id;
			this.matricule = matricule;
			this.login = login;
			this.pass = pass;
			this.nom = nom;
			this.poste = poste;
			this.mail = mail;
			this.dateemb = dateemb;
			this.role = role;
		}


		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getMatricule() {
			return matricule;
		}
		public void setMatricule(String matricule) {
			this.matricule = matricule;
		}
		public String getLogin() {
			return login;
		}
		public void setLogin(String login) {
			this.login = login;
		}
		public String getPass() {
			return pass;
		}
		public void setPass(String pass) {
			this.pass = pass;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPoste() {
			return poste;
		}
		public void setPoste(String poste) {
			this.poste = poste;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public Date getDateemb() {
			return dateemb;
		}
		public void setDateemb(Date dateemb) {
			this.dateemb = dateemb;
		}
		public Role getRole() {
			return role;
		}
		public void setRole(Role role) {
			this.role = role;
		}

    
    
}
