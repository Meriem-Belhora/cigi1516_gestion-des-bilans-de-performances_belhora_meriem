package Model;




public class Qualification {

	
	private int id_qualif;
	private String qualif;
	private int poid;
	
	
	
	
	public Qualification() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Qualification(int id_qualif, String qualif, int poid) {
		super();
		this.id_qualif = id_qualif;
		this.qualif = qualif;
		this.poid = poid;
	}
	public int getId_qualif() {
		return id_qualif;
	}
	public void setId_qualif(int id_qualif) {
		this.id_qualif = id_qualif;
	}
	public String getQualif() {
		return qualif;
	}
	public void setQualif(String qualif) {
		this.qualif = qualif;
	}
	public int getPoid() {
		return poid;
	}
	public void setPoid(int poid) {
		this.poid = poid;
	}

   
	

}
