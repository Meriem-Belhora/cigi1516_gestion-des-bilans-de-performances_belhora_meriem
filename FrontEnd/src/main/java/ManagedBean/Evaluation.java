package ManagedBean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import Model.Qualification;

@ManagedBean
@RequestScoped
public class Evaluation {

	private int id;
	private String Evaluation;
	private Qualification qualif;
	List<Qualification> qualis;
	private String Remarque;

	@PostConstruct
	void init() {
		ClientConfig config = new DefaultClientConfig();
		Client c = Client.create(config);
		String url = "http://localhost:8080/testJersey/webapi/myresource/qualif";
		WebResource resource = c.resource(url);
		ClientResponse response = resource.accept("application/json").get(ClientResponse.class);
		List<Qualification> qualis = response.getEntity(new GenericType<List<Qualification>>() {
		});

		System.out.println(qualis.size());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEvaluation() {
		return Evaluation;
	}

	public void setEvaluation(String evaluation) {
		Evaluation = evaluation;
	}

	public Qualification getQualif() {
		return qualif;
	}

	public void setQualif(Qualification qualif) {
		this.qualif = qualif;
	}

	public List<Qualification> getQualis() {
		return qualis;
	}

	public void setQualis(List<Qualification> qualis) {
		this.qualis = qualis;
	}

	public String getRemarque() {
		return Remarque;
	}

	public void setRemarque(String remarque) {
		Remarque = remarque;
	}

}
